<?php 
require_once("nusoap/lib/nusoap.php");
require_once("funcion.php");

//instancia del server
$server = new soap_server();

//Contiene el nombre del webservice el segundo un namespace
$server->ConfigureWSDL('Prueba','urn:Prueba');

	//Medico
	//se define un tipo complejo (arreglo asociativo, ='struct')
	//para devolverun arreglo
	//estructura a dos niveles no se puede definir en sola una definicion de ComplexType
	//se tiene que definir cada nivel como un ComplexType:
	$server->wsdl->addComplexType('medico','complexType','struct','all',''
		array(
			'idmedico' => array('idmedico'=>'idmedico','type'=>'xsd:string'),
			'nombre' => array('nombre'=>'nombre','type'=>'xsd:string'),
			'especialidad' => array('especialidad'=>'especialidad','type'=>'xsd:string'),
			'idsala' => array('idsala'=>'idsala','type'=>'xsd:string')
		)
	);

	//se define el arreglo que va a contener estos struct:

	$server->wsdl->addComplexType('medicoArray','complexType','array','','SOAP-ENC:Array'
		array (),
		array(
			array(
				'ref' =>'SOAP-ENC:arrayType',
				'wsdl:arrayType' =>'tns:medico[]'
			)
		)
);


	//registro de la funcion listarMedico

	$server->register(
		'listarMedico', //nombre del medico
		array('datos' => 'xsd:string'),//parametro de entrada
		array('result' => 'xsd:bool', 'medico' => 'tns:alumnsArray', 'error' => 'xsd:string'), //valores de retorno
		'urn:PruebaWSDL',//nombre del namespace
		'urn:PruebaWSDL#listarMedico' //accion SOAP
	);

	//registro de la function buscar alumno
	$server->register(
		'buscarMedico',
		array('idmedico' =>'xsd:string'),
		array('result' =>'XSD:bool','medico' => 'tns:medicoArray', 'error' => 'xsd:string'),
		'urn:PruebaWSDL',
		'urn:PruebaWSDL#buscarMedico'
	);

	//registro de la funcion nuevoMedico
	$server->register(
		'nuevoMedico'
		array("idmedico"=>"xsd:string", "nombre"=>"xsd:string", "especialidad"=>"xsd:string", "idsala"=>"xsd:string"),
		array('return' =>'xsd:boolean'),
			'urn:PruebaWSDL',
			'urn:PruebaWSDL#nuevoMedico'
	);

	//registro de la funcion actualizarMedico

	$server->register(
		'actualizarMedico'
		array("idmedico"=>"xsd:string", "nombre"=>"xsd:string", "especialidad"=>"xsd:string", "idsala"=>"xsd:string"),
		array('return' => 'xsd:boolean'),
			'urn:PruebaWSDL',
			'urn:PruebaWSDL#actualizarMedico'
	);

	//permite leer datos POST sin procesar
	@$server->service(file_get_contents("php://input"));


<?